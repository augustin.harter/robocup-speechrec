#JSGF V1.0;
grammar speechRecognition;

public <sentence> = ([tobi | biron | robot] <VP1> [([and|then] <VP2>)] [([and|then] <VP3>)] [([and|then] <VP4>)]);


<VP1> 		= (<VP_tritrans> | <VP_bitrans> | <VP_trans> );
<VP2> 		= (<VP_tritrans> | <VP_bitrans> | <VP_trans> );
<VP3> 		= (<VP_tritrans> | <VP_bitrans> | <VP_trans> );
<VP4> 		= (<VP_tritrans> | <VP_bitrans> | <VP_trans> );

<VP_trans> = <findObj> | <findPrs> | <followPrs> | <gotoLoc> | <speakInfo> | <answer> | <takeObj>;
<VP_bitrans> = <deliverObjToLoc> | <deliverObjToPrs> | <findObjInLoc> | <findPrsInLoc> | <followPrsToLoc> | <takeObjFromLoc> | <speakAboutNumberofPrs> | <guidePrsToLoc> | <followPrsInLoc>;
<VP_tritrans> = <deliverObjToPrsInLoc> | <offerObjToPrsInLoc> | <guidePrsFromLocToLoc>;

<findObj> = <V_find> (<NP_object> | <NP_objectpron>);
<findPrs> = <V_find> (<NP_person> | <NP_personalpron>); 
<followPrs> = <V_follow> (<NP_personalpron>);
<gotoLoc> = <V_goto> <NP_place>;
<speakInfo> = <V_speak> <NP_talk>;
<answer> = <V_answer> <NP_answer>;
<takeObj> = <V_take> (<NP_object> | <NP_objectpron>);

<deliverObjToLoc> = <V_deliver> (<NP_object> | <NP_objectpron>) <toLocPrep> <NP_place>;
<deliverObjToPrs> = <V_deliver> (<NP_object> | <NP_objectpron>) <deliverPrep> (<NP_person> | <NP_personalpron>);
<findObjInLoc> = <V_find> (<NP_object> | <NP_objectpron>) <objInLocPrep> <NP_place>;
<findPrsInLoc> = <V_find> (<NP_person> | <NP_personalpron>) <prsInLocPrep> <NP_place>;
<followPrsToLoc> = <V_follow> (<NP_personalpron>) <toLocPrep> <NP_place>;
<followPrsInLoc> = <V_follow> (<NP_person> | <NP_personalpron>) <prsInLocPrep>  <NP_place>;
<takeObjFromLoc> = <V_take> (<NP_object> | <NP_objectpron>) <takePrep> <NP_place>;
<speakAboutNumberofPrs> = <V_speakCount> <NP_personCat> <countPrep> <NP_place>;
<guidePrsToLoc> = <V_guide> (<NP_person> | <NP_personalpron>) <toLocPrep> <NP_placeTwo>;

<offerObjToPrsInLoc> = <V_offer> <NP_objectCat> <deliverPrep> [((all the) | the)] <NP_personCat> <prsInLocPrep> <NP_place>;
<deliverObjToPrsInLoc> = <V_deliver> (<NP_object> | <NP_objectpron>) <deliverPrep> (<NP_person> | <NP_personalpron>) <prsInLocPrep> <NP_place>;
<guidePrsFromLocToLoc> = <V_guide> (<NP_person> | <NP_personalpron>) <fromLocPrep> <NP_place> <toLocPrep> <NP_placeTwo>;

<V_deliver> 	= (bring | bear | carry | deliver | fetch | transport | take | give | put | place | hand);
<V_find>	= (find | (look for) | search | locate);
<V_follow> 	= (follow);
<V_goto> 	= ((go (to|into)) | (navigate (to|into)) | reach | (get (to|into)) | (move (to|into)) | (drive (to|into))| enter);
<V_take> 	= (take | grasp | get | (pick [up]) | grab);
<V_speak> 	= (tell | say | speak);
<V_answer> 	= (answer);
<V_offer>   = (offer);
<V_speakCount> = (tell | say | speak) [me] ((how many) | (the number of));
<V_guide> = (escort | guide | take);

<NP_place> 	= (the <N_place>) | (a <N_place>) | (an <N_place>) | (<N_place>);
<NP_placeTwo> 	= (the <N_placeTwo>) | (a <N_placeTwo>) | (an <N_placeTwo>) | (<N_placeTwo>);
<NP_object> 	= (the <N_object>) | (a <N_object>) | (an <N_object>) | (<N_object>);
<NP_person> 	= ((a <N_names>)|( the <N_names>) | (<N_names>)) [<clothing>];
<NP_objectpron>	= <N_objectpron>;
<NP_personalpron> = <N_personalpron>;
<NP_talk>	= <N_talk>;
<NP_answer> = <N_answer>;
<NP_objectCat> = [(a | (something to))] <N_objectCat>;
<NP_personCat> = <N_personCat>;
<NP_pose> = <N_pose>;


<N_place> = (drawer | desk | bed | bedside | bar | cupboard | sink | (side shelf) | bookcase | (dining table) | (tv stand) | (living shelf) | (living table) | cabinet | office | bedroom | kitchen | (living room) | corridor);
<N_placeTwo> = (drawer | desk | bed | bedside | bar | cupboard | sink | (side shelf) | bookcase | (dining table) | (tv stand) | (living shelf) | (living table) | cabinet | office | bedroom | kitchen | (living room) | corridor | exit);

<N_object> = (biscuits | (choco syrup) | (baby sweets) | egg | pretzels | pringles | beer | coconut milk | coke | tea | apple | paprika | (pumpernickel) | shampoo | soap | sponge | cloth | bowl | tray | plate | chips);


<Prep> = from | at | (which is in) | (who is in) | in | to | into;
<objInLocPrep> = at | (which is in) | (which is at) | in | on;
<prsInLocPrep> = at | (who is in) | (who is at) | in;
<toLocPrep> = to | into | on;
<deliverPrep> = to;
<takePrep> = from | (which is in);
<fromLocPrep> = from;
<countPrep> = (are in) | (are at);


<N_personalpron> = her | him ;
<N_objectpron> = it | them;
<N_names> = (emma | taylor | sophia | isabella | ava | robin | emily | angel | madison | charlotte | noah | liam | mason | jacob | william | ethan | michael | alexander | james | daniel | person | (calling person) | (waving person) | me);
<N_talk> = (your name) | (the name of your team) | (the time) | (what time is it) | (the date) | (what day is today) | (what day is tomorrow) | (the day of the month) | (the day of the week) | (a joke) | (something about yourself) | (your teams country);
<N_answer> =  (a question) | (question);
<clothing> = ((with a) | (in a) | (with the) | (in the)) (black | white | yellow | red | blue | green) (shirt);
<N_objectCat> = (drink | candy | snack | food | toiletry | container);
<N_personCat> = (girls | boys | men | women | persons);
<N_pose> = (standing | sitting);
